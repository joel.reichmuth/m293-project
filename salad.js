let saladsInf = [ // Dies ist die json liste
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    }
];

saladsInf.forEach(function (salad) { // mit diesem block werden die salads generiert
    let saladDiv = document.createElement('div');
    saladDiv.className = "saladDiv";
    saladDiv.innerHTML = '<div><img src="' + salad.imageUrl + '" alt="' + salad.name + '" class="image"></div> <p class="Ingredients">' + salad.ingredients + '</p>'
        + '<div><table> <tr> <th> <p class="Sort">' + salad.name + '</p></th> <th>'
        + ' <select name="dressing"><option >Italian dressing</option><option >French dressing</option></select>'
        + ' </th> <th> <p>' + salad.prize + '</p> </th> <th><button onclick="addShoppingCart()"><img class="buttonShoppingCart" src="images/shoppingcart.png" alt="shoppingcart"></button></th> </table></div>'


    document.getElementById("salads").appendChild(saladDiv);


});