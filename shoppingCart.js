var number = window.localStorage.getItem('number', number); // Hier wird die Variabel number aus dem local storage genommen
counter.textContent = number;

function addShoppingCart() { // dies wird ausgeführt falls ein Produkt zum warenkorb hinzugefügt wird

    number = document.getElementById("counter").innerHTML;
    number++; // der counter wird erhöt
    counter.textContent = number;
    window.localStorage.setItem('number', number);
}

function alertShoppingCart() {

    window.localStorage.setItem('number', 0) // der local storage wird zurück gefügt
    counter.textContent = 0; // die lokale variabel wird zurück gesetzt
    alert("Thanks for the order, see you next time :)"); // ein alert wird ausgegeben
}