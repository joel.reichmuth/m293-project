let softdrinksInf = [ // Dies ist die json liste
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": "50cl"
    }
];

softdrinksInf.forEach(function (softdrink) { // mit diesem block werden die getränke generiert
    let softdrinkDiv = document.createElement('div');
    softdrinkDiv.className = "softdrinksDiv";
    softdrinkDiv.innerHTML = '<div><img src="' + softdrink.imageUrl + '" alt="' + softdrink.name + '" class="image"></div>'
        + '<div><table> <tr> <th> <p class="Sort">' + softdrink.name + '</p></th> <th>'
        + ' <select name="size"><option >330ml</option><option >500ml</option><option >1500ml</option></select>'
        + ' </th> <th> <p>' + softdrink.prize + '</p> </th><th><button onclick="addShoppingCart()"><img class="buttonShoppingCart" src="images/shoppingcart.png" alt="shoppingcart"></button></th> </table></div>'


    document.getElementById("softdrinks").appendChild(softdrinkDiv);


});

